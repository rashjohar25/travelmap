package com.hotels.abroad.datatypes;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by Johars on 12/21/2015.
 */
public class ImageFile implements Serializable {
    private String id;
    private byte[] data;
    private Bitmap bitmap;
}
