package com.hotels.abroad.managers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import com.hotels.abroad.interfaces.OnGPSCallBack;

import java.util.List;

/**
 * Created by Johars on 11/5/2015.
 */
public class GPSManager {
    private static final int gpsMinTime = 500;
    private static final int gpsMinDistance = 0;
    private static LocationManager locationManager = null;
    private static LocationListener locationListener = null;
    private static OnGPSCallBack gpsCallBack = null;
    private int accuracy = Criteria.ACCURACY_FINE;
    public GPSManager(int accuracy) {
        this.accuracy = accuracy;
        GPSManager.locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (GPSManager.gpsCallBack != null) {
                    GPSManager.gpsCallBack.OnGPSUpdate(location);
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
    }

    public OnGPSCallBack getGpsCallBack() {
        return GPSManager.gpsCallBack;
    }

    public void setGpsCallBack(OnGPSCallBack callBack) {
        GPSManager.gpsCallBack = callBack;
    }

    public void StartListening(Context context) {
        if (GPSManager.locationManager == null) {
            GPSManager.locationManager = (LocationManager) context.getSystemService(context.LOCATION_SERVICE);
        }
        Criteria criteria = new Criteria();
        criteria.setAccuracy(accuracy);
        criteria.setSpeedRequired(true);
        criteria.setAltitudeRequired(true);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        String bestProvider = GPSManager.locationManager.getBestProvider(criteria, true);
        if (bestProvider != null && bestProvider.length() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            GPSManager.locationManager.requestLocationUpdates(bestProvider, GPSManager.gpsMinTime, GPSManager.gpsMinDistance, GPSManager.locationListener);
        } else {
            final List<String> providers = GPSManager.locationManager.getProviders(true);
            for (final String provider : providers) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                }
                GPSManager.locationManager.requestLocationUpdates(provider, GPSManager.gpsMinTime, GPSManager.gpsMinDistance, GPSManager.locationListener);
            }
        }

    }

    public void stopListening(Context context) {

        try {
            if (GPSManager.locationManager != null && GPSManager.locationListener != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                }
                GPSManager.locationManager.removeUpdates(GPSManager.locationListener);
            }

            GPSManager.locationManager = null;
        }
        catch (final Exception ex)
        {

        }
    }
}
