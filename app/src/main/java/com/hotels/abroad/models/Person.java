package com.hotels.abroad.models;

import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by Johars on 12/21/2015.
 */
public class Person extends AbstractUser {
    public static final String FIRSTNAME="firstname";
    public static final String LASTNAME="lastname";

    private String firstName="";
    private String lastName="";

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Person(ParseUser user)
    {
        super(user);
    }
    public Person() {
        super();
    }
    public Person(String userid, String Token) {
        super(userid,Token);

    }

    @Override
    public void LogOut(SharedPreferences preferences, AppCompatActivity activity, @Nullable View view) {
        super.LogOut(preferences, activity, view);
    }

    @Override
    public void SaveUser(AppCompatActivity context, @Nullable View view) {
        super.SaveUser(context, view);
    }

    @Override
    public void SaveInPreferences(SharedPreferences preferences) {
        super.SaveInPreferences(preferences);
    }

    @Override
    public void RemoveUserFromPreferences(SharedPreferences preferences) {
        super.RemoveUserFromPreferences(preferences);
    }
    public void SaveBasicProfile()
    {
        ParseObject basic=new ParseObject("BasicProfile");
        basic.addUnique(USERID,this.getUserid());
        basic.put(FIRSTNAME,firstName);
        basic.put(LASTNAME,lastName);
        basic.saveInBackground();
    }
    public void SaveProfileInPreferences(SharedPreferences preferences)
    {
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString(FIRSTNAME, firstName);
        editor.putString(LASTNAME, lastName);
        editor.commit();
    }
    public void RemoveProfileFromPreferences(SharedPreferences preferences)
    {
        SharedPreferences.Editor editor=preferences.edit();
        editor.remove(FIRSTNAME);
        editor.remove(LASTNAME);
        editor.commit();
    }
}
