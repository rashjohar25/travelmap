package com.hotels.abroad.models;

import com.google.android.gms.maps.model.LatLng;
import com.hotels.abroad.datatypes.ImageFile;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Johars on 12/21/2015.
 */
public class PostCard implements Serializable {
    private String postcardId;
    private String caption;
    private String details;
    private LatLng location;
    private List<ImageFile> images;
    private List<Person> sharedWith;
}
