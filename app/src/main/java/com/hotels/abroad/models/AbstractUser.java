package com.hotels.abroad.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.Toast;

import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.io.Serializable;

/**
 * Created by Johars on 12/20/2015.
 */
public abstract class AbstractUser implements Serializable{
    public static final String USERNAME="Username";
    public static final String TOKEN="SessionToken";
    public static final String EMAIL="Email";
    public static final String USERID="UserId";

    private String userid="";
    private String email="";
    private String username="";
    private String password="";
    private String sessionToken="";

    public AbstractUser(ParseUser user)
    {
        this.userid=user.getObjectId();
        this.username=user.getUsername();
        this.email=user.getEmail();
        this.sessionToken=user.getSessionToken();
    }
    public AbstractUser() {

    }
    public AbstractUser(String userid, String Token) {
        this.userid=userid;
        this.sessionToken=Token;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public void SaveUser(final AppCompatActivity context,@Nullable final View view)
    {
        ParseUser user=new ParseUser();
        user.setEmail(this.email);
        user.setUsername(this.username);
        user.setPassword(this.password);
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    if (view != null) {
                        Snackbar.make(view, "User Saved Successfully.", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, "User Saved Successfully.", Toast.LENGTH_LONG).show();
                    }
                    context.finish();
                } else {
                    if (view != null) {
                        Snackbar.make(view, "User Creation Failed.\n" + e.getMessage(), Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, "User Creation Failed.\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
    public void LogOut(final SharedPreferences preferences,final AppCompatActivity activity,@Nullable final View view)
    {
        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {

                    if (view != null) {
                        Snackbar.make(view, "User Logged out Successfully.", Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(activity, "User Logged out Successfully.", Toast.LENGTH_LONG).show();
                    }
                    RemoveUserFromPreferences(preferences);
                    activity.recreate();
                } else {
                    if (view != null) {
                        Snackbar.make(view, "Log out Failed.\n" + e.getMessage(), Snackbar.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(activity, "Log out Failed.\n" + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
    public void SaveInPreferences(SharedPreferences preferences)
    {
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString(USERID,this.userid);
        editor.putString(USERNAME, this.username);
        editor.putString(EMAIL, this.email);
        editor.putString(TOKEN, this.sessionToken);
        editor.commit();
    }
    public void RemoveUserFromPreferences(SharedPreferences preferences)
    {
        SharedPreferences.Editor editor=preferences.edit();
        editor.remove(USERID);
        editor.remove(USERNAME);
        editor.remove(EMAIL);
        editor.remove(TOKEN);
        editor.commit();
    }
}
