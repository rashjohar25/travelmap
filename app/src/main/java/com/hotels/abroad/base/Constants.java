package com.hotels.abroad.base;

import android.content.SharedPreferences;

import com.hotels.abroad.models.AbstractUser;
import com.hotels.abroad.models.Person;

/**
 * Created by Johars on 12/20/2015.
 */
public class Constants {
    public static final String TAG="Travel Map";
    public static Boolean IsUSERPresentInPreferences(SharedPreferences preferences)
    {
        if(!preferences.contains(AbstractUser.USERID))
            return false;
        if(!preferences.contains(AbstractUser.USERNAME))
            return false;
        if(!preferences.contains(AbstractUser.EMAIL))
            return false;
        if(!preferences.contains(AbstractUser.TOKEN))
            return false;
        return true;
    }
    public static Person GetUserFromPreferences(SharedPreferences preferences) {
        Person abstractAbstractUser =new Person(preferences.getString(AbstractUser.USERID,""),preferences.getString(AbstractUser.TOKEN,""));
        abstractAbstractUser.setUsername(preferences.getString(AbstractUser.USERNAME, ""));
        abstractAbstractUser.setEmail(preferences.getString(AbstractUser.EMAIL, ""));
        return abstractAbstractUser;
    }
}
