package com.hotels.abroad.interfaces;

import android.location.Location;

/**
 * Created by Johars on 11/5/2015.
 */
public interface OnGPSCallBack {
    public abstract void OnGPSUpdate(Location location);
}
