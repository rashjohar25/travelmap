package com.hotels.abroad.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;

import com.hotels.abroad.R;
import com.hotels.abroad.models.AbstractUser;
import com.hotels.abroad.models.Person;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatEditText text_signup_username,text_signup_email,text_signup_password,text_signup_confirmpassword;
    private AppCompatButton button_signup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        button_signup=(AppCompatButton)findViewById(R.id.button_signup);
        button_signup.setOnClickListener(this);
        text_signup_confirmpassword=(AppCompatEditText)findViewById(R.id.text_signup_confirmpassword);
        text_signup_password=(AppCompatEditText)findViewById(R.id.text_signup_password);
        text_signup_username=(AppCompatEditText)findViewById(R.id.text_signup_username);
        text_signup_email=(AppCompatEditText)findViewById(R.id.text_signup_email);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.button_signup)
        {
            if(IsFormValid())
            {
                Person abstractAbstractUser =new Person();
                abstractAbstractUser.setEmail(text_signup_email.getText().toString());
                abstractAbstractUser.setPassword(text_signup_password.getText().toString());
                abstractAbstractUser.setUsername(text_signup_username.getText().toString());
                abstractAbstractUser.SaveUser(SignUpActivity.this,v);
            }
            else
            {
                Snackbar.make(v,"Please Enter the Valid Data.",Snackbar.LENGTH_LONG).show();
            }
        }
    }
    public Boolean IsFormValid()
    {
        if(text_signup_password.getText().toString().equals("")) return false;
        if(text_signup_confirmpassword.getText().toString().equals("")) return false;
        if(!text_signup_password.getText().toString().equals(text_signup_confirmpassword.getText().toString())) return false;
        if(text_signup_password.getText().toString().length()<6) return false;
        if(text_signup_username.getText().toString().equals("")) return false;
        if(text_signup_email.getText().toString().equals("")) return false;
        return true;
    }
}
