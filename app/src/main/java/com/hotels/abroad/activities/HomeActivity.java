package com.hotels.abroad.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceFilter;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hotels.abroad.R;
import com.hotels.abroad.base.Constants;
import com.hotels.abroad.base.DirectionsJSONParser;
import com.hotels.abroad.managers.GPSManager;
import com.hotels.abroad.models.AbstractUser;
import com.hotels.abroad.models.Person;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private GoogleMap mMap;
    private GPSManager gpsManager = null;
    private Intent intent;
    private GoogleApiClient mGoogleApiClient;
    private Boolean isLocationRecieved = false;
    private LatLng currentLocation = new LatLng(0.0f, 0.0f);
    private Person loggedIn;
    private List<Place> nearByPlacestoMe=new LinkedList<Place>();
    private Place searchedPlace=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View headerView = inflater.inflate(R.layout.nav_header_home, null);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();
        MenuItem item = menu.findItem(R.id.nav_login);
        if (Constants.IsUSERPresentInPreferences(preferences)) {
            item.setTitle("Log out");
            loggedIn = Constants.GetUserFromPreferences(preferences);
            ((TextView) headerView.findViewById(R.id.text_header_email)).setText(loggedIn.getEmail());
            ((TextView) headerView.findViewById(R.id.text_header_username)).setText(loggedIn.getUsername());
            navigationView.addHeaderView(headerView);
        } else {
            item.setTitle("Login");
        }
        navigationView.setNavigationItemSelectedListener(this);
        gpsManager = new GPSManager(Criteria.ACCURACY_FINE);
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                searchedPlace=place;
                SetMarker(mMap,searchedPlace.getLatLng().latitude,searchedPlace.getLatLng().longitude,true,BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE),searchedPlace.getName().toString(),searchedPlace.getAddress().toString());
            }

            @Override
            public void onError(Status status) {

            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mGoogleApiClient = getGoogleClient();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_settings) {
            intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_login) {
            if (item.getTitle().toString().equals("Login")) {
                intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            } else {
                loggedIn.LogOut(preferences, this, item.getActionView());
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setBuildingsEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mMap.clear();
                GetPlaces(mMap);
                if(searchedPlace!=null)
                {
                    SetMarker(mMap,searchedPlace.getLatLng().latitude,searchedPlace.getLatLng().longitude,true,BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE),searchedPlace.getName().toString(),searchedPlace.getAddress().toString());
                }
                List<LatLng> ll = new LinkedList<LatLng>();
                ll.add(currentLocation);
                ll.add(marker.getPosition());
                String url = getDirectionsUrl(currentLocation, marker.getPosition());
                DownloadTask downloadTask = new DownloadTask();
                downloadTask.execute(url);
                return false;
            }
        });

        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                if (!isLocationRecieved) {
                    currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    CameraPosition.Builder builder = new CameraPosition.Builder();
                    builder.target(currentLocation);
                    builder.tilt(90.0f);
                    builder.zoom(17.0f);
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
                    isLocationRecieved = true;
                }
                GetPlaces(mMap);
            }
        });

    }
    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.connect();
        super.onStop();
    }
    
    private GoogleApiClient getGoogleClient()
    {
        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(this);
        builder.addApi(Places.GEO_DATA_API);
        builder.addApi(Places.PLACE_DETECTION_API);
        builder.addConnectionCallbacks(this);
        builder.addOnConnectionFailedListener(this);
        return builder.build();
    }
    private void SetMarker(GoogleMap map,double latitude,double longitude,Boolean zoom,@Nullable BitmapDescriptor icon,@Nullable String title,@Nullable String snippet)
    {
        LatLng location=new LatLng(latitude,longitude);
        MarkerOptions placemarker = new MarkerOptions();
        placemarker.position(location);
        if(icon!=null)
        {
            placemarker.icon(icon);
        }
        if(title!=null)
        {
            placemarker.title(title);
        }
        if(snippet!=null)
        {
            placemarker.snippet(snippet);
        }
        map.addMarker(placemarker);
        if(zoom)
        {
            CameraPosition.Builder builder = new CameraPosition.Builder();
            builder.target(location);
            builder.zoom(17.0f);
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
        }
    }
    private void GetPlaces(final GoogleMap map)
    {
            if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi.getCurrentPlace(mGoogleApiClient, null);
            result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                @Override
                public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                    Place place;
                    for (int i = 0; i < likelyPlaces.getCount(); i++) {
                        place = likelyPlaces.get(i).getPlace();
                        nearByPlacestoMe.add(place);
                        SetMarker(map, place.getLatLng().latitude, place.getLatLng().longitude, false, BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN), place.getName().toString(), place.getAddress().toString());
                    }
                    likelyPlaces.release();
                }
            });
        if(searchedPlace!=null) {
            SetMarker(mMap, searchedPlace.getLatLng().latitude, searchedPlace.getLatLng().longitude, false, BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE), searchedPlace.getName().toString(), searchedPlace.getAddress().toString());
        }

    }
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = result.get(i);
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }
                lineOptions.addAll(points);
                lineOptions.width(24);
                lineOptions.color(Color.BLUE);
            }
            mMap.addPolyline(lineOptions);
        }
    }
    private class DownloadTask extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try{
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.e(Constants.TAG,e.toString());
            }
            return data;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){
        String str_origin = "origin="+origin.latitude+","+origin.longitude;
        String str_dest = "destination="+dest.latitude+","+dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin+"&"+str_dest+"&"+sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
        return url;
    }
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb  = new StringBuffer();
            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        }catch(Exception e){
            Log.e(Constants.TAG, e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
}
